package pl.sda.course.SDA_13_GitHub_repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

//@Component - base stereotype, means that Spring will create bean based on this class
@Data
@Entity
public class GitRepoData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE) //disable lombok's setter due db manage it
    @JsonIgnore
    private Long id; //this is id for our database

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id")
    private OwnerRepositoryData owner;
    @JsonProperty("full_name")
    @Column(unique = true)
    private String name; //name of github repository
    private String description;
    private String url;
    @JsonProperty("id")
    private Long repoId; //this is id from github
    @JsonProperty("watchers_count")
    private Long watchersCount;
    @JsonIgnore
    private String error; //field not exist in Json, for error information purposes
}
