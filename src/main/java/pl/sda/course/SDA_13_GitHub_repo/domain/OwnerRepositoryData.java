package pl.sda.course.SDA_13_GitHub_repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * General data of repository owner
 */
//@Component - base stereotype, means that Spring will create bean based on this class
@Data
@Entity
public class OwnerRepositoryData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id; //this is id for our database
    @Column(unique = true)
    private String login;
    @JsonProperty("site_admin")
    private boolean siteAdmin;
    @JsonProperty("id")
    private Long ownerId; //this is id from github
}
