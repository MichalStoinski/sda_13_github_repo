package pl.sda.course.SDA_13_GitHub_repo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Personal data for author and committer
 */
//@Component - base stereotype, means that Spring will create bean based on this class
@Data
@Entity
public class PersonalData implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Setter(AccessLevel.NONE) //disable lombok's setter due db manage it
    @JsonIgnore
    private Long id;

    private String name;
    private String email;
}
