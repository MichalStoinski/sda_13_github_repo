package pl.sda.course.SDA_13_GitHub_repo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.course.SDA_13_GitHub_repo.domain.CommitGeneralData;

import java.util.List;

/**
 * Repository interface - if there's no methods signature, then use only inherited methods.
 * Chain of inheritance: JpaRepository < PagingAndSortingRepository < CrudRepository < Repository
 * Spring convention: methods name consist of SQL syntax - Spring translate it into SQL query's
 *
 * As a convention: interface name ends with "Repository", place it into "repository" package,
 * it extends one of Spring Data repository interface and annotate it with @Repository
 */
@Repository
public interface CommitGeneralDataRepository extends JpaRepository<CommitGeneralData, Long> {

    List<CommitGeneralData> getAllByUrlContaining(String urlPart);

    // TODO throws NullPointerException
    boolean existsByUrlContaining(String urlPart);
}
