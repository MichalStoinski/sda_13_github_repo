package pl.sda.course.SDA_13_GitHub_repo.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.sda.course.SDA_13_GitHub_repo.domain.CommitGeneralData;
import pl.sda.course.SDA_13_GitHub_repo.domain.GitRepoData;
import pl.sda.course.SDA_13_GitHub_repo.errorHandling.SDAException;
import pl.sda.course.SDA_13_GitHub_repo.repository.CommitGeneralDataRepository;
import pl.sda.course.SDA_13_GitHub_repo.repository.GitRepoDataRepository;
import pl.sda.course.SDA_13_GitHub_repo.repository.OwnerRepositoryDataRepository;

import java.util.Arrays;
import java.util.List;

/**
 * Slave class (secondary) * Service class - stereotype shows that class offers some business logic, used in other classes, such as controllers
 * By default is Singleton
 */
@Service
public class GitRepoService {
    private final static String GIT_API_URL = "https://api.github.com/repos/{userName}/{repositoryName}";
    private static final String GIT_API_URL_COMMITS = GIT_API_URL + "/commits";

    private RestTemplate restTemplate;
    private GitRepoDataRepository gitRepoDataRepository;
    private CommitGeneralDataRepository commitGeneralDataRepository;
    private OwnerRepositoryDataRepository ownerRepositoryDataRepository;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired //not necessary, but good practise
    public GitRepoService(RestTemplate restTemplate, GitRepoDataRepository gitRepoDataRepository,
                          CommitGeneralDataRepository commitGeneralDataRepository, OwnerRepositoryDataRepository ownerRepositoryDataRepository) {
        this.restTemplate = restTemplate;
        this.gitRepoDataRepository = gitRepoDataRepository;
        this.commitGeneralDataRepository = commitGeneralDataRepository;
        this.ownerRepositoryDataRepository = ownerRepositoryDataRepository;
    }


    /**
     * Returns data of user repository, using Github API url
     * Deserialize to POJO with Jackson (Jackson is a high-performance JSON processor for Java)
     *
     * @param userName       repository user name
     * @param repositoryName repository name
     * @return Github repository data
     */
    @Transactional //TODO add comment
    public GitRepoData getRepositoryByUserAndRepoName(String userName, String repositoryName) {
        try {
            GitRepoData gitRepoData;
            //parsing user name and repository name to database's format
            String userAndRepoName = getString(userName, repositoryName);
            if (gitRepoDataRepository.existsByName(userAndRepoName)) {
                gitRepoData = gitRepoDataRepository.getByName(userAndRepoName);
            } else {
                gitRepoData = restTemplate.getForObject(GIT_API_URL, GitRepoData.class, userName, repositoryName);
                if (ownerRepositoryDataRepository.existsByLogin(gitRepoData.getOwner().getLogin())){
                    gitRepoData.setOwner(ownerRepositoryDataRepository.getByLogin(gitRepoData.getOwner().getLogin()));
                }
                gitRepoDataRepository.save(gitRepoData);
            }
            return gitRepoData;
        } catch (HttpClientErrorException ex) {
            return getGitRepoErrorData(ex);
        }
    }

    /**
     * Returns commits from user repository, using Github API url
     *
     * @param userName       repository user name
     * @param repositoryName repository name
     * @return List of 3 last commits
     */
    public List<CommitGeneralData> getCommitsListByUserAndRepoName(String userName, String repositoryName) {
        try {
            //parsing user name and repository name to database's format
            String userAndRepoName = getString(userName, repositoryName);
            if (commitGeneralDataRepository.existsByUrlContaining(userAndRepoName)) {
                return commitGeneralDataRepository.getAllByUrlContaining(userAndRepoName);
            } else {
                CommitGeneralData[] commitsArrayFromUrl = restTemplate.getForObject(GIT_API_URL_COMMITS,
                        CommitGeneralData[].class, userName, repositoryName);
                List<CommitGeneralData> commitGeneralDataList = Arrays.asList(commitsArrayFromUrl);
                // if there's less or exact 3 commits then return all of them, else return only 3
                // assumpt that list is sorted by commits order
                commitGeneralDataList =
                        commitGeneralDataList.size() <= 3 ? commitGeneralDataList : commitGeneralDataList.subList(0, 3);
                commitGeneralDataRepository.saveAll(commitGeneralDataList);
                return commitGeneralDataList;
            }
        } catch (HttpClientErrorException ex) {
            throw new SDAException(ex.getMessage());
        }
    }

    public void testKafka(){
        kafkaTemplate.send("default", "exists");
    }

    @KafkaListener(topics = {"default"})
    public void testReceivingKafka(ConsumerRecord<?,?> consumerRecord){

    }


    private static String getString(String userName, String repositoryName) {
        return String.format("%s/%s", userName, repositoryName);
    }

    /**
     * Create new GitRepoData and put error message getting from exception message
     *
     * @param ex exception
     * @return GitRepoData with error message
     */
    private GitRepoData getGitRepoErrorData(HttpClientErrorException ex) {
        GitRepoData errorData = new GitRepoData(); //create new, empty object to set its error field
        errorData.setError(ex.getMessage());
        return errorData;
    }
}
