package pl.sda.course.SDA_13_GitHub_repo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.sda.course.SDA_13_GitHub_repo.domain.CommitGeneralData;
import pl.sda.course.SDA_13_GitHub_repo.domain.GitRepoData;
import pl.sda.course.SDA_13_GitHub_repo.service.GitRepoService;

import java.util.List;

/**
 * Master (primary) class. Calls methods from Service.
 */
@Controller
public class GetDataController {

    private static final String URL_TO_GET_REPOSITORY = "/getRepository/{userName}/{repositoryName}";
    private static final String URL_TO_GET_COMMITS = URL_TO_GET_REPOSITORY + "/commits";
    private GitRepoService gitRepoService;

    @Autowired
    public GetDataController(GitRepoService gitRepoService) {
        this.gitRepoService = gitRepoService;
    }

    // generic type must be Object, because at error only the message is returned
    @GetMapping(URL_TO_GET_REPOSITORY)
    public ResponseEntity<Object> getRepositoryByUserAndRepoName(
            @PathVariable("userName") String userName, @PathVariable("repositoryName") String repositoryName) {
        GitRepoData response = gitRepoService.getRepositoryByUserAndRepoName(userName, repositoryName);
        if (response.getError() != null) {
            return new ResponseEntity<>(response.getError(), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/testKafka")
    public void testKafka() {
        gitRepoService.testKafka();
    }

    // cannot resolve path variables in request mapping, while URL is set as a concatenation of final strings
    @GetMapping(URL_TO_GET_COMMITS)
    public ResponseEntity<Object> getCommitsListByUserAndRepoName(
            @PathVariable("userName") String userName, @PathVariable("repositoryName") String repositoryName) {
        List<CommitGeneralData> commitList = gitRepoService.getCommitsListByUserAndRepoName(userName, repositoryName);
        return new ResponseEntity<>(commitList, HttpStatus.OK);
    }
}
