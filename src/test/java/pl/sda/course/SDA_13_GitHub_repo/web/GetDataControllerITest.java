package pl.sda.course.SDA_13_GitHub_repo.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.sda.course.SDA_13_GitHub_repo.Sda13GitHubRepoApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Sda13GitHubRepoApplication.class)
@AutoConfigureMockMvc
public class GetDataControllerITest {

    private static final String URL_TO_REPO_DATA = "/getRepository/{owner}/{repo}";
    private static final String URL_TO_COMMITS = URL_TO_REPO_DATA + "/commits";
    private static final String USER_LOGIN = "lukasz-bacic";
    private static final String USER_NAME = "lbacic";
    private static final String REPOSITORY_NAME = "java5pozHibernate";
    private static final String NOT_EXISTING_USER = "not_existing_user";
    private static final String NOT_FOUND = "404 Not Found";

    @Autowired
    MockMvc mockMvc;

    @Test
    public void shouldReturnValidResponse() throws Exception {

        mockMvc.perform(get(URL_TO_REPO_DATA, USER_LOGIN, REPOSITORY_NAME))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.owner.login").
                        value(USER_LOGIN));
    }

    @Test
    public void shouldReturnNotValidResponse() throws Exception {

        mockMvc.perform(get(URL_TO_REPO_DATA, NOT_EXISTING_USER, REPOSITORY_NAME))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$")
                        .value(NOT_FOUND));
    }

    @Test
    public void shouldReturnValidCommitList() throws Exception {

        mockMvc.perform(get(URL_TO_COMMITS, USER_LOGIN, REPOSITORY_NAME))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].commit.author.name").
                        value(USER_NAME));
    }

    @Test
    public void shouldReturn403ErrorWhenAskingForCommits() throws Exception {

        mockMvc.perform(get(URL_TO_COMMITS, NOT_EXISTING_USER, REPOSITORY_NAME))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$")
                        .value(NOT_FOUND));
    }
}